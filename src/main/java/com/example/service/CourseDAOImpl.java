package com.example.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.dao.CourseDAO;
import com.example.model.CourseModel;

@Service
public class CourseDAOImpl implements CourseDAO {
	
	@Bean
	public RestTemplate RestTemplateCourse() 
	{
		return new RestTemplate(); // Return an instance
	}
	
	@Autowired  
	private RestTemplate restTemplateCourse = this.RestTemplateCourse();

	@Override
	public CourseModel selectCourse(String id) {
		// TODO Auto-generated method stub
		CourseModel course = restTemplateCourse.getForObject("http://localhost:8080/rest/course/view/"+id, CourseModel.class);         
		return course;
	}

	@Override
	public List<CourseModel> selectAllCourse() {
		// TODO Auto-generated method stub
		CourseModel[] courses = restTemplateCourse.getForObject("http://localhost:8080/rest/course/viewall", CourseModel[].class);
	    return Arrays.asList(courses);
	}

}
