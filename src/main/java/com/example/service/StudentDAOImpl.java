package com.example.service;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.example.dao.StudentDAO;
import com.example.model.StudentModel;

@Service
public class StudentDAOImpl implements StudentDAO {

	@Bean
	public RestTemplate RestTemplate() 
	{
		return new RestTemplate(); // Return an instance
	}
	
	@Autowired  
	private RestTemplate restTemplate = RestTemplate();
	
	@Override
	public StudentModel selectStudent (String npm)     
	{         
		StudentModel student = restTemplate.getForObject("http://localhost:8080/rest/student/view/"+npm, StudentModel.class);         
		return student;  } 
	 
	@Override     
	public List<StudentModel> selectAllStudents ()  
	{   
		StudentModel[] students = restTemplate.getForObject("http://localhost:8080/rest/student/viewall", StudentModel[].class);
	    return Arrays.asList(students); 
	}

}
